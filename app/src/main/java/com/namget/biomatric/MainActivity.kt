package com.namget.biomatric


import android.app.KeyguardManager
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.*
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.namget.biomatric.databinding.ActivityMainBinding
import java.util.concurrent.Executor

class MainActivity : AppCompatActivity() {

    // 늦은 초기화를 이용하여 binding을 불러온다.
    //private lateinit var binding: ActivityMainBinding
    private lateinit var executor: Executor
    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.button).setOnClickListener {
            if (checkForBiometrics()) {
                runFingerprint()
            }
        }
    }

    private fun checkForBiometrics() : Boolean{
        Log.d(TAG, "checkForBiometrics started")
        var canAuthenticate = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT < 29) {
                val keyguardManager : KeyguardManager = applicationContext.getSystemService(KEYGUARD_SERVICE) as KeyguardManager
                val packageManager : PackageManager = applicationContext.packageManager
                if(!packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
                    Log.w(TAG, "checkForBiometrics, Fingerprint Sensor not supported")
                    Toast.makeText(applicationContext, "checkForBiometrics, Fingerprint Sensor not supported", Toast.LENGTH_SHORT).show()
                    canAuthenticate = false
                }
                if (!keyguardManager.isKeyguardSecure) {
                    Log.w(TAG, "checkForBiometrics, Lock screen security not enabled in Settings")
                    Toast.makeText(applicationContext, "checkForBiometrics, Lock screen security not enabled in Settings", Toast.LENGTH_SHORT).show()
                    canAuthenticate = false
                }
            } else {
                val biometricManager : BiometricManager = BiometricManager.from(applicationContext)//applicationContext.getSystemService(BiometricManager::class.java)
                val biometricRetCode = biometricManager.canAuthenticate(BIOMETRIC_STRONG or DEVICE_CREDENTIAL or BIOMETRIC_WEAK)
                Log.d(TAG, "biometricRetCode: $biometricRetCode")
                if(biometricRetCode != BiometricManager.BIOMETRIC_SUCCESS){
                    Log.w(TAG, "checkForBiometrics, biometrics not supported")
                    Toast.makeText(applicationContext, "checkForBiometrics, biometrics not supported", Toast.LENGTH_SHORT).show()
                    canAuthenticate = false
                }
            }
        }else{
            canAuthenticate = false
        }
        Log.d(TAG, "checkForBiometrics ended, canAuthenticate=$canAuthenticate ")
        return canAuthenticate
    }

    private fun runFingerprint() {
        val biometricPromptInfo: BiometricPrompt.PromptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle("finger print biomatric")
                .setDescription("namget tistory blog")
                .setSubtitle("namget")
                .setNegativeButtonText("Cancel")
                .build()
        val authenticationCallback = getAuthenticationCallback()
        executor = ContextCompat.getMainExecutor(this)
        val biometricPrompt: BiometricPrompt = BiometricPrompt(this, executor, authenticationCallback)
        biometricPrompt.authenticate(biometricPromptInfo)
    }


    private fun getAuthenticationCallback() = object : BiometricPrompt.AuthenticationCallback() {
        override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
            super.onAuthenticationError(errorCode, errString)
            Toast.makeText(applicationContext, "인증 에러", Toast.LENGTH_SHORT).show()
        }

        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
            super.onAuthenticationSucceeded(result)
            Toast.makeText(baseContext, "인증 성공", Toast.LENGTH_SHORT).show()
            Log.d("", "인증 성공")
        }

        override fun onAuthenticationFailed() {
            super.onAuthenticationFailed()
            Toast.makeText(applicationContext, "인증 실패", Toast.LENGTH_SHORT).show()
        }
    }
}
